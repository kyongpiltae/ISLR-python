import numpy as np
import tensorflow as tf
import os
import cv2
import math
import _pickle as cPickle
import time

from lib.roi_data_layer.layer import RoIDataLayer
from lib.tiny.config import cfg

class SolverWrapper(object):
    def __init__(self, sess, network, roidb, output_dir,
                 centers, pretrained_model=None):
        """Initialize the SolverWrapper."""
        self.net = network
        self.roidb = roidb
        self.output_dir = output_dir
        self.pretrained_model = pretrained_model
        self.centers = centers

        # For checkpoint
        self.saver = tf.train.Saver(max_to_keep=100)

    def snapshot(self, sess, iter, epoch):
        # This part handle the checkpoint (ckpt) generation of the network
        net = self.net

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        filename = (cfg.TRAIN.SNAPSHOT_PREFIX + '_' +
                    '_epoch_{:d}'.format(epoch+1))
        filename = os.path.join(self.output_dir, filename)

        self.saver.save(sess, filename, global_step=iter)
        print('Wrote snapshot to: {:s}'.format(filename))

    def train_model(self, sess, epochs = 1, iter_per_epoch = 100, restore=False):
        """Network training loop."""

        # Pass to RoIDataLayer for dataset input and pre-processing
        data_layer = get_data_layer(self.roidb, self.centers, 1)

        # Refer to 'network.py' loss building function for training
        loss, det_loss, huber_loss = self.net.build_loss()

        with tf.variable_scope("optimizer", reuse=tf.AUTO_REUSE) as scope:
            # lr is for training CNN network and score_res4, lr2 is for training CNN network and score_res3
            # lr and lr2 need to be different learning rate (important!)
            lr = tf.Variable(cfg.TRAIN.LEARNING_RATE, trainable=False)
            lr2 = tf.Variable(cfg.TRAIN.LEARNING_RATE * 0.1, trainable=False)
            momentum = cfg.TRAIN.MOMENTUM
            opt = tf.train.MomentumOptimizer(lr, momentum)
            opt2 = tf.train.MomentumOptimizer(lr2, momentum)

            global_step = tf.Variable(0, trainable=False)
            
            tvars = tf.trainable_variables()
            # CNN network and score_res4, need to change to other parameter name if using different pre-trained network.
            opt_var1 = [var for var in tvars if 'score_res3' not in var.name]
            # CNN network and score_res3, need to change to other parameter name if using different pre-trained network.
            opt_var2 = [var for var in tvars if 'score_res3' in var.name]
            # Two optimizer together to train the network.
            train_op1 = opt.minimize(loss, var_list=opt_var1, global_step=global_step)
            train_op2 = opt2.minimize(loss, var_list=opt_var2, global_step=global_step)
            train_op = tf.group(train_op1, train_op2)

        # intialize variables
        sess.run(tf.global_variables_initializer())
        restore_iter = 0

        filename = os.path.join(self.output_dir, "ResNet101_pretrained")
        print("Restoring from ... %s" %filename)
        self.saver.restore(sess, filename)
        # load pre-trained CNN network.
        if self.pretrained_model is not None and not restore:
            try:
                print ('Loading pretrained model weights from {:s}'.format(self.pretrained_model))
                filename = os.path.join(self.output_dir, "ResNet101_pretrained")
                self.saver.save(sess, filename)
                self.net.load(self.pretrained_model, sess, True)
                self.saver.save(sess, filename)
            except:
                raise 'Check your pretrained model {:s}'.format(self.pretrained_model)

        # Resuming a trainer if restore=True, need to specify a ckpt directory
        if restore:
            try:
                ckpt = tf.train.get_checkpoint_state(self.output_dir)
                print('Restoring from {}...'.format(ckpt.model_checkpoint_path))
                self.saver.restore(sess, ckpt.model_checkpoint_path)
                stem = os.path.splitext(os.path.basename(ckpt.model_checkpoint_path))[0]
                restore_iter = int(stem.split('-')[-1])
                sess.run(global_step.assign(restore_iter))
                print('done')
            except:
                print('Failed to find a checkpoint!')
                sys.exit(1)

        last_snapshot_iter = -1
        restore_epoch = int((restore_iter+1) / iter_per_epoch)

        # Training Iteration
        for epoch in range(restore_epoch, epochs):
            for e_iter in range(0, iter_per_epoch):
                start = time.time()
                iter = epoch*(iter_per_epoch) + e_iter

                # Geting next minibatch, refer to 'roi_data_layer' directory
                blobs = data_layer.forward()

                feed_dict={
                    self.net.data: blobs['data'],
                    self.net.clsmap: blobs['clsmap'],
                    self.net.regmap: blobs['regmap']}

                res_fetches = [self.net.get_output('score_cls'),
                               self.net.get_output('score_reg'),
                               self.net.get_output('prob_cls')]


                fetch_list = [loss, det_loss, huber_loss, train_op] + res_fetches
                fetch_list += []

                # Where we feed input into tensor graph and obtain result
                loss_value, det_loss_value, huber_loss_value, _, \
                score_cls_np, score_reg_np, prob_cls_np = sess.run(fetches=fetch_list, feed_dict=feed_dict)

                _diff_time = time.time()-start

                # Printing training information in terminal
                if (iter) % (cfg.TRAIN.DISPLAY) == 0:
                    print('Epoch: [%02d/%02d], iter: [%d/%d], Loss: %.4f'%\
                            ((epoch+1), epochs, (e_iter+1), iter_per_epoch, loss_value))
                    print('speed: {:.3f}s / iter'.format(_diff_time))

                # Save a checkpoint every epoch
                if (iter+1) % iter_per_epoch == 0:
                    last_snapshot_iter = iter
                    self.snapshot(sess, iter, epoch)

        # Saving final checkpoint before ending
        if last_snapshot_iter != iter:
            self.snapshot(sess, iter)

#==================================================================================================================================

def get_training_roidb(pkl_file):
    # This part only served as loading .pkl file
    with open(pkl_file, 'rb') as fid:
        roidb = cPickle.load(fid)
    assert (len(roidb) > 0), 'No data loaded!'
    print('Loaded roidb done!')
    return roidb

def get_data_layer(roidb, centers, num_classes):
    # RoIDataLayer will handle both input and pre-processing of training dataset
    layer = RoIDataLayer(roidb, centers, num_classes)
    return layer

def train_net(network, roidb, output_dir, ref_Box, pretrained_model=None, epochs=1, iter_per_epoch = 100, restore=False):
    """Train a tiny face detection network."""

    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        sw = SolverWrapper(sess, network, roidb, output_dir, centers=ref_Box, pretrained_model=pretrained_model)
        print('Start training...')
        sw.train_model(sess, epochs, iter_per_epoch, restore=restore)
        print('Training done!')
