import numpy as np
from easydict import EasyDict as edict

__C = edict()
# Consumers can get config by:
#   from fast_rcnn_config import cfg
cfg = __C

#
# Training options
#
__C.TRAIN = edict()
__C.TRAIN.BATCH_SIZE = 8
__C.TRAIN.FLIPPED = True
__C.TRAIN.SCALING = True
__C.TRAIN.SCALING_FACTER = [0.5, 1, 2]
__C.TRAIN.POS_IOU_Thresh = 0.7
__C.TRAIN.NEG_IOU_Thresh = 0.3
__C.TRAIN.NMS_Thresh = 0.1
__C.TRAIN.CONFIDENCE_Thresh = 0.5
__C.TRAIN.CROP_SIZE = 500
__C.TRAIN.VAR_SIZE = 8
__C.TRAIN.NORMALIZE = True
__C.TRAIN.SOLVER = 'Momentum'
__C.TRAIN.WEIGHT_DECAY = 0.0005
__C.TRAIN.LEARNING_RATE = 0.0001
__C.TRAIN.MOMENTUM = 0.9
__C.TRAIN.GAMMA = 0.1
__C.TRAIN.STEPSIZE = 16100
__C.TRAIN.DISPLAY = 1
__C.TRAIN.LOG_IMAGE_ITERS = 805
__C.TRAIN.SAMPLE_LIMIT = 128
__C.TRAIN.PRUNING = True
__C.TRAIN.SNAPSHOT_PREFIX = 'Resnet101'

#
# Common options
#
__C.RGB_MEANS = np.array([119.29960, 110.54627, 101.83843], dtype=np.float32)
__C.RGB_VARIANCE = np.array([[0.74215591, -1.3868568, 0.69416434],
                            [2.6491976, 0.088368624, -2.6558025],
                            [7.3054428, 7.6848936, 7.5429802]], dtype=np.float32)
