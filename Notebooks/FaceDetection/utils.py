import os
from time import strftime, localtime

def get_output_dir(output_dir, network_name):
    outdir = os.path.abspath(os.path.join(os.path.dirname(__file__), output_dir))
    time = strftime("%m-%d-%H-%M", localtime())
    if network_name is not None:
        n_name = network_name.split('_')[0]
        #outdir = os.path.join(outdir, (n_name + '_' + time))
        outdir = os.path.join(outdir, n_name)
    #else:
        #outdir = os.path.join(outdir, time)
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    return outdir

def get_log_dir(log_dir):
    logdir = os.path.abspath(os.path.join(os.path.dirname(__file__), log_dir))
    logdir = os.path.join(logdir, strftime("%Y-%m-%d-%H-%M-%S", localtime()))
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    return log_dir



######################### plot related utility functions ###########################
import numpy as np
import matplotlib.pyplot as plt
import cv2
from matplotlib import cm
import math

def normalizer_blob(im, rgb_means):
    # Basically just subtracted the already cropped image with means.
    blobs_data = {'data' : None}
    norm = im.astype(np.float32, copy=True)
    offset = rgb_means.copy()
    # Swaping offset from RGB to BGR (to match cv2 channel order)
    offset[0], offset[2] = offset[2], offset[0]
    norm = norm - np.tile(offset, (norm.shape[0], im.shape[1], 1))
    # Generating a empty np array in blob
    img_shape = norm.shape
    blob = np.zeros((1, img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    # Fitting normalized image to blob
    blob[0, 0:img_shape[0], 0:img_shape[1], :] = norm
    blobs_data['data'] = blob
    return blobs_data

def plot_score_colorbar(im, output_dest, dpi=80):
    height = im.shape[0]
    width = im.shape[1]
    fig, ax = plt.subplots(figsize=(width/float(dpi), height/float(dpi)), dpi=dpi)
    vis = ax.imshow(cv2.cvtColor(im, cv2.COLOR_BGR2RGB))
    cbar = fig.colorbar(vis, ax=ax, 
                        ticks=np.linspace(0, 255, 5), 
                        pad=0.01, fraction=0.03, 
                        shrink=0.95, orientation='vertical')
    _ = cbar.ax.set_yticklabels(np.linspace(0.0, 1.0, 5), fontsize=int(1.5*height/dpi))
    _ = plt.tight_layout()
    _ = plt.axis('off')
    plt.savefig(os.path.splitext(output_dest)[0], dpi=dpi, bbox_inches='tight', transparent=True, pad_inches=0)

def vis_detections(im, boxes, conf, output_dest):
    # A visualization of detection
    output_img = im.copy()
    im_shape = im.shape
    # color = (0, 255, 255)
    cmap = cm.viridis
    for ind, box in enumerate(boxes):
        (x1, y1, x2, y2) = box
        x1, y1 = max(x1, 0), max(y1, 0)
        x2, y2 = min(x2, im_shape[1]), min(y2, im_shape[0])
        bw = x2 - x1 + 1
        bh = y2 - y1 + 1
        if (min(bw, bh) <= 20):
            lw = 1
        else:
            lw = int(max(2, min(3, min([bh/20, bw/20]))))
        color = tuple((math.ceil(ch*256)-1) for ch in cmap(conf[ind])[:3])
        color = (color[2], color[1], color[0])
        cv2.rectangle(output_img, (int(x1), int(y1)), (int(x2), int(y2)), color, lw)

    plot_score_colorbar(output_img, output_dest)