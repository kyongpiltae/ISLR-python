import tensorflow as tf
import numpy as np
from sklearn.model_selection import KFold
import sklearn
import time

def calculate_roc(thresholds, embeddings1, embeddings2, actual_issame, nrof_folds=10):
    assert (embeddings1.shape[0] == embeddings2.shape[0])
    assert (embeddings1.shape[1] == embeddings2.shape[1])
    nrof_pairs = min(len(actual_issame), embeddings1.shape[0])
    nrof_thresholds = len(thresholds)
    k_fold = KFold(n_splits=nrof_folds, shuffle=False)

    tprs = np.zeros((nrof_folds, nrof_thresholds))
    fprs = np.zeros((nrof_folds, nrof_thresholds))
    accuracy = np.zeros((nrof_folds))
    indices = np.arange(nrof_pairs)
    
    diff = np.subtract(embeddings1, embeddings2)
    dist = np.sum(np.square(diff), 1)

    for fold_idx, (train_set, test_set) in enumerate(k_fold.split(indices)):
        # Find the best threshold for the fold
        acc_train = np.zeros((nrof_thresholds))
        for threshold_idx, threshold in enumerate(thresholds):
            acc_train[threshold_idx] = calculate_accuracy(threshold, dist[train_set], actual_issame[train_set])
        best_threshold_index = np.argmax(acc_train)
        
        accuracy[fold_idx] = calculate_accuracy(thresholds[best_threshold_index], dist[test_set],
                                                      actual_issame[test_set])

    return accuracy

def calculate_accuracy(threshold, dist, actual_issame):
    predict_issame = np.less(dist, threshold)
    tp = np.sum(np.logical_and(predict_issame, actual_issame))
    fp = np.sum(np.logical_and(predict_issame, np.logical_not(actual_issame)))
    tn = np.sum(np.logical_and(np.logical_not(predict_issame), np.logical_not(actual_issame)))
    fn = np.sum(np.logical_and(np.logical_not(predict_issame), actual_issame))

    acc = float(tp + tn) / dist.size
    return acc

def evaluate(embeddings, actual_issame, nrof_folds=10):
    thresholds = np.arange(0, 4, 0.01)
    embeddings1 = embeddings[0::2] 
    embeddings2 = embeddings[1::2] 

    accuracy = calculate_roc(thresholds, embeddings1, embeddings2,
                                       np.asarray(actual_issame), nrof_folds=nrof_folds)
    return accuracy


def data_iter(datasets, batch_size):
    data_num = datasets.shape[0]
    for i in range(0, data_num, batch_size):
        yield datasets[i:min(i+batch_size, data_num), ...]

def test(data_set, sess, embedding_tensor, label_shape=None, feed_dict=None, input_placeholder=None):
    batch_size = 32
    data_list = data_set[0]
    issame_list = data_set[1]
    embeddings_list = []
    time_consumed = 0.0
    
    datas = data_list[0]
    embeddings = None
    feed_dict.setdefault(input_placeholder, None)
    for idx, data in enumerate(data_iter(datas, batch_size)):
        data_tmp = data.copy()    # fix issues #4
        data_tmp -= 127.5
        data_tmp *= 0.0078125
        feed_dict[input_placeholder] = data_tmp
        time0 = time.time()
        _embeddings = sess.run(embedding_tensor, feed_dict)
        time_now = time.time()
        diff = time_now - time0
        time_consumed += diff
        if embeddings is None:
            embeddings = np.zeros((datas.shape[0], _embeddings.shape[1]))
        try:
            embeddings[idx*batch_size:min((idx+1)*batch_size, datas.shape[0]), ...] = _embeddings
        except ValueError:
            print('idx*batch_size value is %d min((idx+1)*batch_size, datas.shape[0]) %d, batch_size %d, data.shape[0] %d' %
                  (idx*batch_size, min((idx+1)*batch_size, datas.shape[0]), batch_size, datas.shape[0]))
            print('embedding shape is ', _embeddings.shape)
    embeddings_list.append(embeddings)

    embeddings = embeddings_list[0]
    embeddings = sklearn.preprocessing.normalize(embeddings)

    accuracy = evaluate(embeddings, issame_list, nrof_folds=10)
    acc2, std2 = np.mean(accuracy), np.std(accuracy)
    return acc2, std2


def ver_test(ver_list, sess, embedding_tensor, feed_dict, input_placeholder):
    results = []

    for i in range(len(ver_list)):
        acc2, std2 = test(data_set=ver_list[i], sess=sess, embedding_tensor=embedding_tensor,
                                                              feed_dict=feed_dict,
                                                              input_placeholder=input_placeholder)
        print('Validation accuracy: %1.5f+-%1.5f' % (acc2, std2))
        results.append(acc2)
    return results
