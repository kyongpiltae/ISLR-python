# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
import cv2

from .ArcFaceResNet import *

class ArcFaceRecognizer():
    def __init__(self, model_dir = None):
        config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
        self.sess = tf.Session(config=config)
        self.x = tf.placeholder(dtype=tf.float32, shape=[None, 112, 112, 3], name='input_place')
        
        input_checkpoint = model_dir
        dropout_rate = tf.constant(1, dtype = tf.float32)
        self.feature = get_resnet(self.x, num_layers = 50, type='ir', w_init=None, trainable=False, keep_rate=dropout_rate)
        saver = tf.train.Saver(tf.global_variables())
        saver.restore(self.sess, input_checkpoint)
            
    def extract_feature(self, img, flip = True):
        img_tmp = cv2.resize(img, dsize = (112,112), interpolation = cv2.INTER_CUBIC)

        img_tmp = cv2.cvtColor(img_tmp, cv2.COLOR_RGB2BGR)
        data_tmp = img_tmp.copy().astype(np.float32)
        data_tmp -= 127.5
        data_tmp *= 0.0078125

        data_tmp2 = np.fliplr(data_tmp)
        features = self.sess.run(self.feature, feed_dict = {self.x: [data_tmp, data_tmp2]})
        features = features[0] + features[1]

        return features
    
    
    def extract_feature_batch(self, img_batch):
        _img_batch = np.zeros_like(img_batch)
        _img_batch[:,:,:,0] = img_batch[:,:,:,2]
        _img_batch[:,:,:,1] = img_batch[:,:,:,1]
        _img_batch[:,:,:,2] = img_batch[:,:,:,0]
        _img_batch = (_img_batch - 127.5) * 0.0078125
        
        features = self.sess.run(self.feature, feed_dict = {self.x: _img_batch})

        return features
    
         
        

