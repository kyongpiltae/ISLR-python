import tensorflow as tf

def ReLU(_x, name):
    return tf.nn.relu(_x)
 
def conv_block(input_tensor, filters, stage, block, strides=(2, 2), bias=False, w_init = None, training = False):
    layer_name = 'conv' + str(stage) + '_' + str(block)
    l = tf.layers.batch_normalization(input_tensor, axis=3, name=layer_name + '_conv1/bn1', momentum=0.9, epsilon=2e-5, training = training)
    l = tf.layers.conv2d(l, filters[0], 3, strides=strides, padding = "SAME", use_bias=bias, name=layer_name + '_conv1',
                        kernel_initializer = w_init)
    l = tf.layers.batch_normalization(l, axis=3, name=layer_name + '_conv1/bn2', momentum=0.9, epsilon=2e-5, training = training)
    l = ReLU(l,layer_name+"_prelu1/alpha")

    l = tf.layers.conv2d(l, filters[1], 3, padding='SAME', use_bias=bias, name=layer_name + '_conv2',
                        kernel_initializer = w_init)
    l = tf.layers.batch_normalization(l, axis=3, name=layer_name + '_conv2/bn', momentum=0.9, epsilon=2e-5, training = training)
    m = tf.layers.conv2d(input_tensor, filters[2], 1, strides=strides, use_bias=bias, name=layer_name + '_shortcut',
                        kernel_initializer = w_init)
        
    l = tf.add(l, m)

    return l

def identity_block(input_tensor, filters, stage, block, bias=False, last_relu=True, w_init = None, training = False):
    layer_name = 'conv' + str(stage) + '_' + str(block)
    l = tf.layers.batch_normalization(input_tensor, axis=3, name=layer_name + '_conv1/bn1', momentum=0.9, epsilon=2e-5, training = training)
    l = tf.layers.conv2d(l, filters[0], 3, padding='SAME', use_bias=bias, name=layer_name + '_conv1',
                        kernel_initializer = w_init)
    l = tf.layers.batch_normalization(l, axis=3, name=layer_name + '_conv1/bn2', momentum=0.9, epsilon=2e-5, training = training)
    l = ReLU(l,layer_name+"_prelu1/alpha")

    l = tf.layers.conv2d(l, filters[1], 3, padding='SAME', use_bias=bias, name=layer_name + '_conv2',
                        kernel_initializer = w_init)
    l = tf.layers.batch_normalization(l, name=layer_name + '_conv2/bn', momentum=0.9, epsilon=2e-5, training = training)

    l = tf.add(l, input_tensor)
    
    return l


def get_resnet(inputs, num_layers, type=None, w_init=None, trainable=None, keep_rate=None, sess=None):
    training = trainable
    with tf.variable_scope('resnet50', reuse=tf.AUTO_REUSE) as scope:
        # First block:
        l = tf.layers.conv2d(inputs, 64, (3, 3), strides=(1, 1), padding='SAME', use_bias=False, name='conv_in',
                        kernel_initializer = w_init)
        l = tf.layers.batch_normalization(l, axis=3, name='conv_in/bn', momentum=0.9, epsilon=2e-5, training = training)
        l = ReLU(l,"prelu_in/alpha")
        # Second block:
        with tf.variable_scope('block2', reuse=tf.AUTO_REUSE) as scope:
            l = conv_block(l, [64, 64, 64], stage=2, block=1, training = training, w_init = w_init)
            l = identity_block(l, [64, 64, 64], stage=2, block=2, training = training, w_init = w_init)
            l = identity_block(l, [64, 64, 64], stage=2, block=3, training = training, w_init = w_init)

        # Third block:
        with tf.variable_scope('block3', reuse=tf.AUTO_REUSE) as scope:
            l = conv_block(l, [128, 128, 128], stage=3, block=1, training = training, w_init = w_init)
            l = identity_block(l, [128, 128, 128], stage=3, block=2, training = training, w_init = w_init)
            l = identity_block(l, [128, 128, 128], stage=3, block=3, training = training, w_init = w_init)
            l = identity_block(l, [128, 128, 128], stage=3, block=4, training = training, w_init = w_init)

        # Fourth block:
        with tf.variable_scope('block4', reuse=tf.AUTO_REUSE) as scope:
            l = conv_block(l, [256, 256, 256], stage=4, block=1, training = training, w_init = w_init)
            for i in range(2,15):
                l = identity_block(l, [256, 256, 256], stage=4, block=i, training = training, w_init = w_init)

        # Fifth block:
        with tf.variable_scope('block5', reuse=tf.AUTO_REUSE) as scope:
            l = conv_block(l, [512, 512, 512], stage=5, block=1, training = training, w_init = w_init)
            l = identity_block(l, [512, 512, 512], stage=5, block=2, training = training, w_init = w_init)
            l = identity_block(l, [512, 512, 512], stage=5, block=3, last_relu=False, training = training, w_init = w_init)

        # Final stage: BN + dropout + FC + BN
        l = tf.layers.batch_normalization(l, name="final_bn", momentum=0.9, epsilon=2e-5, training = training)
        l = tf.nn.dropout(l, keep_prob=keep_rate, name='final_dropout')
        l = tf.layers.flatten(l)
        l = tf.layers.dense(l, 512, activation=None, name='final_fc', kernel_initializer = w_init)  
        l = tf.layers.batch_normalization(l, momentum=0.9, epsilon=2e-5, training = training)

        l = tf.identity(l, name = "output")
    return l