numpy==1.15.4
matplotlib==3.0.2
scikit-learn==0.22.1
pandas==0.23.4
seaborn==0.9.0
statsmodels==0.10.2
xlrd==1.2.0
patsy==0.5.1
ipython==7.6.1
scipy==1.2.0
hmmlearn==0.2.3
tensorflow-gpu==1.11.0
cython==0.29.7 #FaceDetection
easydict==1.9 #FaceDetection
imageio==2.6.1 #FaceRecognition
opencv-python==3.4.5.20 #FaceRecognition
imutils==0.5.3 #Medicine
pillow==5.4.1 #Medicine
